---
layout: home

hero:
  name: "pzm9012 的文稿"
  text: "几篇零零散散的文章的发布地"
  tagline: 🔄注意，本站更新可能落后于 GitHub 文档站
  actions:
    - theme: brand
      text: 进入
      link: /home
    - theme: alt
      text: 转到 deepin 常用资源整理
      link: /deepin-src-col/sec-0
    - theme: alt
      text: 转到 deepin 高频问题解决方案整理
      link: /deepin-solutions/论坛发帖提问须知
---
